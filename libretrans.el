;;; libretrans.el --- Access Libretranslate instances  -*- lexical-binding: t; -*-

;; Copyright (C) 2021 marty hiatt

;; Author: marty hiatt <martianhiatus [a t] riseup [d o t] net>
;; Homepage: https://codeberg.org/martianh/libretrans.el
;; Package-Requires: ((emacs "25.1"))
;; Version: 0.2
;; Keywords: convenience, translation, wp, text

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Interact with Libretranslate instances from within Emacs. See the readme
;; for more information.

;;; Code:

(require 'json)
(require 'mm-url)
(require 'url)

(when (require 'pdf-tools nil :no-error)
  (declare-function pdf-view-active-region-text "pdf-view"))

(defgroup libretrans nil
  "Interact with Libretranslate instances."
  :prefix "libretrans"
  :group 'external)

(defcustom libretrans-instance "https://libretranslate.de"
  "The libretrans instance to use."
  :type 'string)

;; TODO: let user choose these from libretrans-languages
(defcustom libretrans-source nil
  "The default language to translate from, as a two letter code.
If nil, query the server to detect the language.
For details of what languages are availble and their
corresponding codes, see `libretrans-languages'."
  :type 'string)

(defcustom libretrans-target "en"
  "The default language to translate to, as a two letter code.
For details of what languages are availble and their
corresponding codes, see `libretrans-languages'."
  :type 'string)

(defvar libretrans-languages nil)

(defvar libretrans-languages-backup
  '(("en" . "English")
    ("ar" . "Arabic")
    ("az" . "Azerbaijani")
    ("zh" . "Chinese")
    ("cs" . "Czech")
    ("da" . "Danish")
    ("nl" . "Dutch")
    ("eo" . "Esperanto")
    ("fi" . "Finnish")
    ("fr" . "French")
    ("de" . "German")
    ("el" . "Greek")
    ("he" . "Hebrew")
    ("hi" . "Hindi")
    ("hu" . "Hungarian")
    ("id" . "Indonesian")
    ("ga" . "Irish")
    ("it" . "Italian")
    ("ja" . "Japanese")
    ("ko" . "Korean")
    ("fa" . "Persian")
    ("pl" . "Polish")
    ("pt" . "Portuguese")
    ("ru" . "Russian")
    ("sk" . "Slovak")
    ("es" . "Spanish")
    ("sv" . "Swedish")
    ("tr" . "Turkish")
    ("uk" . "Ukranian")
    ("vi" . "Vietnamese")))

(defun libretrans-languages-url ()
  "The URL for a libretrans source and target languages list query."
  (concat libretrans-instance "/languages"))

(defvar libretrans-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "l") #'libretrans-translate)
    map)
  "Keymap for libretrans results buffer.")

(defun libretrans--get-languages ()
  "Return the languages supported by the server."
  (let* ((url-request-method "GET")
         (response-buffer (url-retrieve-synchronously
                           (libretrans-languages-url) t))
         (json-object-type 'alist)
         (json-array-type 'list)
         (json-key-type 'string))
    (with-current-buffer response-buffer
      (goto-char (point-min))
      (search-forward "\n\n")
      (json-read))))

(defun libretrans--return-langs-as-list ()
  "Return a list of cons cells containing languages supported by the server."
  (let* ((langs-response (libretrans--get-languages)))
    (mapcar (lambda (x)
              (cons (cdar x)
                    (cdadr x)))
            langs-response)))

(defun libretrans-update-libretrans-languages ()
  "Set `libretrans-languages' to the data returned by the server."
  (interactive)
  (setq libretrans-languages (or (libretrans--return-langs-as-list)
                                 libretrans-languages-backup)))

(defun libretrans--get-query-region ()
  "Get current region for default search."
  (if (equal major-mode 'pdf-view-mode)
      (when (region-active-p)
        (pdf-view-active-region-text))
    (when (use-region-p)
      (buffer-substring-no-properties (region-beginning) (region-end)))))

(defun libretrans--get-text-query (text region)
  "Get the text query to send.
\nEither from TEXT, or REGION, or current word, or user input."
  (or text
      (read-string
       (format "Libretranslate (%s): " (or region (current-word) ""))
       nil nil (or region (current-word)))))

(defun libretrans--read-lang (source-or-target langs)
  "Read a language from list LANGS.
\n SOURCE-OR-TARGET is whether the language selected is a source
language or target language."
  (let ((response
         (completing-read (format "%s language: "
                                  (upcase-initials
                                   (symbol-name source-or-target)))
                          langs)))
    (alist-get response langs nil nil #'equal)))

(defun libretrans--reverse-langs (langs)
  "Reverse the alist of LANGS."
  (mapcar (lambda (x)
            (cons (cdr x) (car x)))
          langs))

(defun libretrans--detect-lang-request (text)
  "Send a POST request of TEXT to detect its language."
  (let* ((url-request-method "POST")
         (url-request-extra-headers
          '(("Content-Type" . "application/x-www-form-urlencoded")
            ("accept" . "application/json")))
         (form-data `(("q" . ,text)))
         (url-request-data
          (mm-url-encode-www-form-urlencoded form-data)))
    (url-retrieve-synchronously
     (concat libretrans-instance
             "/detect"))))

(defun libretrans--detect-lang (text)
  "Return the code of the language detected by the server.
TEXT is the search query to detect with."
  (let ((json-array-type 'list)
        (response (libretrans--detect-lang-request text)))
    (with-current-buffer response
      (let ((json (libretrans--translate-process-json)))
        (alist-get 'language (car json))))))

;;;###autoload
(defun libretrans-translate (&optional arg text variable-pitch)
  "Prompt for TEXT to translate and return the translation in a buffer.
By default, in order, text is given as a second argument, the
current region, the word at point, or user input. With a single
prefix ARG, prompt to specify a source language different to the
value detected by the server. With a second prefix ARG, promp to
to specify both a source language different to
`libretrans-source' and a target language different to
`libretrans-target'."
  (interactive "P")
  (let* ((url-request-method "POST")
         (url-request-extra-headers
          '(("Content-Type" . "application/x-www-form-urlencoded")
            ("accept" . "application/json")))
         (region (libretrans--get-query-region))
         (text (libretrans--get-text-query text region))
         (libretrans-langs-reversed
          (if (not (boundp 'libretrans-languages))
              (progn (libretrans-update-libretrans-languages)
                     (libretrans--reverse-langs libretrans-languages))
            (libretrans--reverse-langs libretrans-languages)))
         (libretrans-source-temp
          (if (and arg (>= (car arg) 4)) ; if 1 or 2 prefix args
              (libretrans--read-lang 'source libretrans-langs-reversed)
            (or libretrans-source ; use if customized, else:
                (libretrans--detect-lang text))))
         (libretrans-target-temp
          (if (equal arg '(16))         ; only if 2 prefix args
              (libretrans--read-lang 'target libretrans-langs-reversed)
            libretrans-target))
         ;; we now encode below with `mm-url-encode-www-form-urlencoded':
         ;; (query text) ;(url-hexify-string text))
         (form-data `(("q" . ,text)
                      ;; ,(url-http--encode-string query))
                      ("source" . ,libretrans-source-temp)
                      ("target" . ,libretrans-target-temp)))
         (url-request-data
          (mm-url-encode-www-form-urlencoded form-data)))
    (url-retrieve
     (concat libretrans-instance "/translate")
     (lambda (_status)
       (apply #'libretrans--translate-callback
              (libretrans--translate-process-json)
              (libretrans--status)
              (list variable-pitch libretrans-source-temp libretrans-target-temp))))))

(defun libretrans--translate-callback (json &optional status variable-pitch source target)
  "Display the translation returned in JSON in a buffer.
\nWhen VARIABLE-PITCH is non-nil, activate `variable-pitch-mode'.
SOURCE and TARGET and the languages translated to and from."
  (cond ((equal 'error (caar json))
         (error "Error - %s" (alist-get 'error json)))
        ((/= 200 (string-to-number status))
         (error "Error: request returned HTTP %s." status))
        (t
         (with-current-buffer (get-buffer-create "*libretrans*")
           (let ((inhibit-read-only t)
                 (json-processed
                  (alist-get 'translatedText json)))
             (special-mode)
             (erase-buffer)
             (insert json-processed)
             (kill-new json-processed)
             (message "Translation copied to clipboard.")
             (switch-to-buffer-other-window (current-buffer))
             (libretrans-mode)
             (visual-line-mode)
             ;; handle borked filling:
             (when variable-pitch (variable-pitch-mode 1))
             (setq-local header-line-format
                         (propertize
                          (format "Libretrans translation from %s to %s:"
                                  (alist-get source libretrans-languages
                                             nil nil #'equal)
                                  (alist-get target libretrans-languages
                                             nil nil #'equal))
                          'face font-lock-comment-face))
             (goto-char (point-min)))))))

(defun libretrans--translate-process-json ()
  "Parse the JSON from the HTTP response."
  (goto-char (point-min))
  (re-search-forward "^$" nil 'move)
  (let ((json-string
         (decode-coding-string
          (buffer-substring-no-properties (point) (point-max))
          'utf-8)))
    (json-read-from-string json-string)))

;; process the http response:
(defun libretrans--response ()
  "Capture response buffer content as string."
  (with-current-buffer (current-buffer)
    (buffer-substring-no-properties (point-min) (point-max))))

(defun libretrans--response-body (pattern)
  "Return substring matching PATTERN from `libretrans--response'."
  (let ((resp (libretrans--response)))
    (string-match pattern resp)
    (match-string 0 resp)))

(defun libretrans--status ()
  "Return HTTP Response Status Code from `libretrans--response'."
  (let* ((status-line (libretrans--response-body "^HTTP/1.*$")))
    (string-match "[0-9][0-9][0-9]" status-line)
    (match-string 0 status-line)))

(define-minor-mode libretrans-mode
  "Minor mode for libretrans results buffer.")

(provide 'libretrans)
;;; libretrans.el ends here
